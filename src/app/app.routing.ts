import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login.component';
import { RegisterComponent } from './components/register.component';
import {DefaultComponent} from './components/default.component';
import {UserEditComponent} from "./components/user.edit.component";
import {TaskNewComponent} from "./components/task.new.component";
import {TaskDetailComponent} from "./components/task.detail.component";

//creamos las rutas predefinidas al entrar en la app

const appRoutes: Routes = [

  {path: 'login',           component: LoginComponent},
  {path: 'index',           component: DefaultComponent},
  {path: 'index/:page',     component: DefaultComponent},
  {path: 'register',        component: RegisterComponent},
  {path: 'login/:id',       component: LoginComponent},//ruta recibe un numero
  {path: 'user-edit',       component: UserEditComponent},
  {path: 'new-task',        component: TaskNewComponent},
  {path: 'task/:id',        component: TaskDetailComponent},
  {path: '',                component: DefaultComponent},
  {path: '**',              component: LoginComponent}, //si no existe la ruta

];


//cartgar en el modulo del router

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
