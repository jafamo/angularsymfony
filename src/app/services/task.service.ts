import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import "rxjs/add/operator/map"; // capturar servicio REST
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class TaskService {
  public url: string;

  constructor(
    private _http: Http
  ){
    this.url = GLOBAL.url; // saca la URL de nuestra API
  }

  create(token, task){

    let json = JSON.stringify(task);
    let params = "json="+json+'&authorization='+token;
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this._http.post(this.url+'/task/new',params,{headers:headers}).map(res => res.json());
  }

  getTasks(token, page = null){
    //let params = 'authentication='+token;
    let params = "authorization="+token;
    //
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    if(page == null ){
      page = 1;
    }
    //console.log('aqui el page del getTask');
  //console.log(page);
    //peticion AJAx
    return this._http.post(this.url+ '/task/list?page='+page, params, {headers:headers}).map(res => res.json());

  }

  /**Clase 68
   *
   * @param token
   * @param id
   */
  getTask(token,id){
    let params = "authorization="+token;
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    return this._http.post(this.url + '/task/detail/' + id, params,{headers:headers}).map(res => res.json());

  }


}
