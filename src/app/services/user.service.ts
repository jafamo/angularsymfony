import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import "rxjs/add/operator/map";
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';


@Injectable()
export class UserService {

  public url: string;
  public identity;
  public token;

  constructor(private _http: Http){
    this.url = GLOBAL.url;
  }

  signup(user_to_login){
    let json = JSON.stringify(user_to_login);
    let params = "json="+json;

    //definir los headers de la peticion:
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
   return this._http.post(this.url + '/login', params, {headers: headers}).map(res => res.json());


  }

  /**Sacar información identity del LocalStorage
   *
   */
  getIdentity(){
    let identity = JSON.parse(localStorage.getItem('identity'));
    if(identity != "undefined"){
      this.identity = identity;
    }else{
      this.identity = null;
    }
    return identity;
  }

  /**Obtener el token almacenado en el LocalStorage
   *
   */
  getToken(){
    let token = JSON.parse(localStorage.getItem('token'));
    if(token){
      this.token = token;
    }else{
      this.token = null;
    }
    return token;

  }

  register(user_to_register){
    let json = JSON.stringify(user_to_register);
    let params = "json="+json;
    //debugger
    //cabeceras
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    //convertimos el objeto a json
    return this._http.post(this.url + '/user/new',params, {headers:headers}).map(res => res.json())
  }

  //para actualizar el user, tiene que saber que usuario esta logueado
  //para eso le pasamos el jsnon + el authorization
  update_user(user_to_update){
    let json = JSON.stringify(user_to_update);
    let params = "json="+json+'&authorization='+this.getToken();
    let headers = new Headers({'Content-Type':'application/x-www-form-urlencoded'});
    return this._http.post(this.url+ '/user/edit',params,{headers:headers}).map(res => res.json());

  }


}
