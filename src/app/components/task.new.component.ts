import { Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute, Params, Route} from '@angular/router';
import { UserService} from '../services/user.service';
import { TaskService} from '../services/task.service';
import {Task} from "../models/task";

@Component({
  selector:'task-new',
  templateUrl: '../views/task.new.html',
  providers: [UserService, TaskService]
})

export class TaskNewComponent implements OnInit{
  public titulo:string;
  public identity;
  public estado;
  public token;
  public task: Task;


  constructor(
    private _userService:UserService,
    private _taskService:TaskService,
    private _route: ActivatedRoute,
    private _router: Router
  ){
    this.titulo = "Nueva tarea";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }


  ngOnInit(){
    if(this.identity == null && !this.identity.sub){
      this._router.navigate(['/login']);
    }else{
          console.log("Entrando en el componente de nueva area.");
          this.task = new Task(1,"","","new",'','');
    }


    //console.log(this._taskService.create());
  }

  onSubmit(){
    console.log(this.task);
    this._taskService.create(this.token,this.task).subscribe(
      response => {
        this.estado = response.status;
        if(this.estado != 'success'){
          this.estado = 'error';
        }else{
          this.task = response.data;

          //redirrigiremos a otra
          //window.location.href = '/';
          this._router.navigate(['/']);
        }

    },
      error => {
      console.log(<any>error);
      }
    );

  }
}
