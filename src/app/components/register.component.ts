import { Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute, Params, Route} from '@angular/router';
import { User} from '../models/user';
import { UserService} from '../services/user.service';

@Component({
    selector: 'register',
    templateUrl: '../views/register.html',
    providers: [UserService]
})

export class RegisterComponent implements OnInit{

    public title: string;
    public user: User;
    public status;

    constructor(
       // private _route: ActivatedRoute,
       // private router: Router,
      private _userService: UserService
    ){
        this.title = 'Formulario de registro';
        this.user = new User(100,"user","","","","");
    }

    ngOnInit(){
        console.log('Entrando en el componente Register');
    }

  /**
   * Funcion ara crear el usuario
   *
   */
  onSubmit(){
     //TODO  mostrar error en la validacion del usuario
      console.log(this.user);
      this._userService.register(this.user).subscribe(
        response => {
          console.log(response);
                this.status = response.status;
                console.log(response.status);
                if(response.status != 'success') {
                  this.status = 'error';
                  console.log("No se puede crear usuario");
                }
               // }else{
                //  this.user = new User(100,"user","","","","");
                //  this.status = 'success';
                //}
              },
          error => {
                  console.log(<any>error);
               }
      );
    }
}
