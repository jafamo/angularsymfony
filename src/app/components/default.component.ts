import { Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute, Params, Route} from '@angular/router';
import { UserService} from '../services/user.service';
import { TaskService} from '../services/task.service';
import {Task} from "../models/task";

@Component({
  selector: 'default',
  templateUrl: '../views/default.html',
  providers: [UserService,TaskService]
})

export class DefaultComponent implements OnInit{

  public title: string;
  public identity;
  public token;
  public tasks: Array<Task>;
  public pages;
  public pagePrev;
  public pageNext;
  public loading;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _taskService: TaskService
  ){
    this.title = 'Componente default';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();

  }

  ngOnInit(){
    //console.log('Entrando en el componente Default');
    this.getAllTasks();
    console.log("Entra en el getall");
    //console.log(this.getAllTasks());
  }


  getAllTasks(){
  //saber en que pagina estamos
    this._route.params.forEach((params: Params) => {
        let page = +params['page'];

      //console.log("Pagina:"+page);
        if(!page){
          page = 1;
        }

        this.loading = 'show';
      //console.log("Imprimir la pagina:"+page);

        this._taskService.getTasks(this.token, page).subscribe(
          response =>{
            console.log(response.status);
                    if(response.status == 'succcess' ){
                        this.tasks = response.data;
                        //console.log(this.tasks);
                        //console.log(page);
                      this.loading = 'hide';
                        // Hacer la paginacion
                      this.pages = [];
                      for(let i =0; i< response.total_pages; i++){
                        this.pages.push(i);
                      }

                      //Pagina anterior
                      if(page >=2 ){
                        this.pagePrev = (page -1);
                      }else{
                        this.pagePrev = page;
                      }
                      // Pagina siguiente
                      if(response.page < response.total_pages || response.page == 1){
                        this.pageNext = (page+1);
                      }else{
                        this.pageNext = page;

                      }

                    }
          },
          error => {
            console.log(<any>error);
          }
        );
    });
  }

}
