import { Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../services/user.service';

@Component({
    selector: 'login',
    templateUrl: '../views/login.html',
  //tener el objeto del services sin tener que instanciar en otros sitios
  providers:[UserService]
})

export class LoginComponent implements OnInit{

    public title: string;
    public user;
    public identity;
    public token;



    constructor(

      private _route: ActivatedRoute, private _router: Router,
      private _userService: UserService
    ){
        this.title = 'Componente de Login';
        this.user = {
          "email": "",
          "password": "",
          "getHash": "false"
        };
    }

    ngOnInit(){

      console.log('Entrando en el  componente de Login');
      console.log(this._userService.getIdentity());
      console.log(this._userService.getToken());

      this.logout();
      this.redirectIfIdentity();

    }

  /**
   * Unset variables idenity, token and redirect to login page
   */
  logout(){
      //recoger las variables de la URL
      this._route.params.forEach((params: Params) => {
        let logout = params['id']
        if(logout == 1) {
          localStorage.removeItem('identity');
          localStorage.removeItem('token');
          this.identity = null;
          this.token = null;

          window.location.href = '/login';
        }
      });

    }

  /**Accede al localStorage
   *
   */

  redirectIfIdentity(){
      let identity = this._userService.getIdentity();
      if(identity !=null && identity.sub){
        this._router.navigate(["/"]);
      }
    }



    onSubmit(){
      console.log(this.user);

      this._userService.signup(this.user).subscribe(
          response => {
                  this.identity = response;

            if(this.identity.length <= 1){
              console.log("Error en el servidor de login.component");
            }{
              if(!this.identity.status){
                localStorage.setItem('identity',JSON.stringify(this.identity));

                //GET TOKEN
                this.user.getHash = null;
                this._userService.signup(this.user).subscribe(
                  response => {
                          this.token = response;

                    if(this.identity.length <= 1){
                      console.log("Error en el server");
                    }{
                      if(!this.identity.status){
                        localStorage.setItem('token',JSON.stringify(this.token));
                        window.location.href = "/";
                      }
                    }
                  },
                  error => {
                    console.log("Aqui es donde se para");
                  }
                );

              }
            }
          },
        error => {
              console.log("SHIT..."+<any>error);
      }
      );
     // alert(this._userService.signup());
    }

}
