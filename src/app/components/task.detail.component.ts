import { Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute, Params, Route} from '@angular/router';
import { UserService} from '../services/user.service';
import { TaskService} from '../services/task.service';
import {Task} from "../models/task";

@Component({
  selector:'task-detail',
  templateUrl: '../views/task.detail.html',
  providers: [UserService, TaskService]
})

export class TaskDetailComponent implements OnInit{

  public identity;
  public token;
  public task:Task;
  public loading;

  constructor(
    private _userService: UserService,
    private _taskService: TaskService,
    private _route: ActivatedRoute,
    private _router: Router
  ){
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();

  }

  ngOnInit(): void {
    if(this.identity && this.identity.sub){
      //llamada al servicio de tareas para sacar una tarea
      this.getTask();
    }else{
      this._router.navigate(['/login']);
    }
  }

  /**
   *
    */
  getTask(){
    this.loading = 'show';
    //recorrer params de la URL:
    this._route.params.forEach((params:Params) =>{
      let id = +params['id'];
      console.log('aqui esta el ID tarea:'+id);
      this._taskService.getTask(this.token, id).subscribe(
        response => {
          //solo se muestran tareas de un unico usuario
          console.log(response.tarea.user.id);
          if(response.status == 'success'){

            if(response.tarea.user.id == this.identity.sub){
              this.loading = 'hide';
              this.task = response.tarea;
            }else{
              this._router.navigate(['/']);
            }
          }else{
            console.log('salir de la tarea 2');
            this._router.navigate(['/login']);
          }

        },
        error => {
          console.log(<any>error);
        }
      );
      }

    );
  }
}
