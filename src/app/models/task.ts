export class Task{

  /**Todas las opciones de las tareas
   *
   * @param id
   * @param title
   * @param description
   * @param status
   * @param createdAt
   * @param updateAt
   */
  constructor(
    public id: number,
    public title: string,
    public description: string,
    public status: string,
    public createdAt,
    public updateAt
  ){}
}
